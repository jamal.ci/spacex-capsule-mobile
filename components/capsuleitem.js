import React from "react";
import { StyleSheet, Text, TouchableOpacity } from "react-native";

export default function CapsuleItem({ capsule, pressHandler }) {

    return (
        <TouchableOpacity onPress={() => pressHandler(capsule.id, capsule.serial)}>
            <Text style={styles.item}> {capsule.serial}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    item: {
      marginTop: 16,
      padding: 16,
      borderColor: '#bbb',
      borderWidth: 1,
      borderStyle: 'dashed',
      borderRadius: 10,
      backgroundColor: '#ccc'
    },
  });
  