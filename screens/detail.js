import { StatusBar } from 'expo-status-bar';
import { useState, useEffect } from 'react';
import { Image, StyleSheet, Text, View, SafeAreaView, TouchableOpacity } from 'react-native';



export default function Detail({navigation, route}) {
  const base_url = 'https://api.spacexdata.com/v4/launches/5eb87cdeffd86e000604b330';
  const [launche, setLaunche] = useState({});

  useEffect(() => {
    fetchLaunche();
  }, []);
  
  const fetchLaunche = async () => {
    let response = await (await fetch(base_url)).json();
    setLaunche(response);
    console.log('XXXXXX ', response.links.patch.small);
  };
  
  return (
    <SafeAreaView style={styles.container}>
        <View style={styles.container}>
            {launche ?
            <View>
                <Image
                    source={{uri: 'https://images2.imgbox.com/04/6e/kniggvWD_o.png'}}
                    style={styles.thumbnail}
                />
                <View style={styles.rightContainer}>
                    <Text style={styles.title}>{launche.name}</Text>
                    <Text style={styles.year}>{launche.date_unix}</Text>
                    <Text>flight number: {launche.flight_number}</Text>
                </View>
                <StatusBar style="auto" />
            </View>
            :<Text>Rien</Text>}
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
      },
      rightContainer: {
        flex: 1,
      },
      title: {
        fontSize: 20,
        marginBottom: 8,
        textAlign: 'center',
      },
      thumbnail: {
        //width: 53,
        height: 81,
      },
      listView: {
        paddingTop: 20,
        backgroundColor: '#F5FCFF',
      },
});
