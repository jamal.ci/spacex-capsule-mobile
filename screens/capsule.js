import { StatusBar } from 'expo-status-bar';
import { useState, useEffect } from 'react';
import { FlatList, StyleSheet, Text, View, SafeAreaView, TouchableOpacity } from 'react-native';
import CapsuleItem from '../components/capsuleitem';



export default function Capsule({navigation}) {
  const base_url = 'https://api.spacexdata.com/v4/capsules';
  const [capsules, setCapsules] = useState([]);

  useEffect(() => {
    fetchCapsules();
  }, []);

  const pressHandler = (id_capsule, serial) => {
    // setCapsules((prevCapsules) => {
    //   return prevCapsules.filter(capsule => capsule.id != id_capsule);
    // });
    console.log('capsule: ', id_capsule);
    navigation.navigate('Detail', {name: 'Detaila '+serial});
  };
  
  const fetchCapsules = async () => {
    let response = await (await fetch(base_url)).json();
    setCapsules(response);
    console.log('response ', response);
  };
  
  return (
    <SafeAreaView style={styles.container}>
    <View style={styles.container}>
      <FlatList
        data={capsules}
        renderItem={({item})=>(
          <CapsuleItem capsule={item} pressHandler={pressHandler} />
        )}
        keyExtractor={(item) => item.id}
      />
      <StatusBar style="auto" />
    </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center'
  },
});
